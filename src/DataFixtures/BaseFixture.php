<?php
declare(strict_types=1);

namespace App\DataFixtures;

use DavidBadura\FakerMarkdownGenerator\FakerProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

/**
 * Класс BaseFixture
 *
 * @package App\DataFixtures
 */
abstract class BaseFixture extends Fixture
{
    /**
     * @var Generator
     */
    protected $faker;
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var array
     */
    private $referencesIndex = [];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->faker->addProvider(new FakerProvider($this->faker));

        $this->loadData($manager);
    }

    /**
     * @param ObjectManager $manager
     *
     * @return mixed
     */
    abstract protected function loadData(ObjectManager $manager);

    /**
     * @param string $className
     * @param int    $count
     *
     * @return array
     * @throws \Exception
     */
    protected function getRandomReferences(string $className, int $count): array
    {
        $references = [];

        while (count($references) < $count) {
            $references[] = $this->getRandomReference($className);
        }

        return $references;
    }

    /**
     * @param string $className
     *
     * @return object
     * @throws \Exception
     */
    protected function getRandomReference(string $className): object
    {
        if (!isset($this->referencesIndex[$className])) {
            $this->referencesIndex[$className] = [];

            foreach ($this->referenceRepository->getReferences() as $key => $reference) {
                if (0 === strpos($key, $className.'_')) {
                    $this->referencesIndex[$className][] = $key;
                }
            }
        }

        if (empty($this->referencesIndex[$className])) {
            throw new \Exception(sprintf('Cannot find any references for class "%s"', $className));
        }

        return $this->getReference(
            $this->faker->randomElement($this->referencesIndex[$className])
        );
    }

    /**
     * Create many objects at once:
     *
     *      $this->createMany(10, function(int $i) {
     *          $user = new User();
     *          $user->setFirstName('Ryan');
     *
     *           return $user;
     *      });
     *
     * @param string   $groupName Tag these created objects with this group name,
     *                            and use this later with getRandomReference(s)
     *                            to fetch only from this specific group.
     * @param int      $count
     * @param callable $factory
     */
    protected function createMany(string $groupName, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; $i++) {
            $entity = $factory($i);
            if (null === $entity) {
                throw new \LogicException(
                    'Did you forget to return the entity object from your callback to BaseFixture::createMany()?'
                );
            }
            $this->manager->persist($entity);
            // store for usage later as groupName_#COUNT#
            $this->addReference(sprintf('%s_%d', $groupName, $i), $entity);
        }
    }
}