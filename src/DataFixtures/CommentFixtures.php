<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Класс CommentFixture
 *
 * @package App\DataFixtures
 */
class CommentFixtures extends BaseFixture implements DependentFixtureInterface
{
    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            ArticleFixtures::class,
        ];
    }

    /**
     * @param ObjectManager $manager
     *
     * @return mixed|void
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Comment::class,
            100,
            function () {
                $comment = new Comment();

                $comment->setAuthorName($this->faker->name)
                    ->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'))
                    ->setArticle($this->getRandomReference(Article::class))
                    ->setIsDeleted($this->faker->boolean(20))
                    ->setContent(
                        $this->faker->boolean ? $this->faker->paragraph : $this->faker->sentences(2, true)
                    );

                return $comment;
            }
        );
        $manager->flush();
    }
}
