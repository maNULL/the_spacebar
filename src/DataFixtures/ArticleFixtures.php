<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Tag;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Класс ArticleFixtures
 *
 * @package App\DataFixtures
 */
class ArticleFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $articleTitles = [
        'Why Asteroids Taste Like Bacon',
        'Life on Planet Mercury: Tan, Relaxing and Fabulous',
        'Light Speed Travel: Fountain of Youth or Fallacy',
    ];
    private static $articleImages = [
        'asteroid.jpeg',
        'mercury.jpeg',
        'lightspeed.png',
    ];
    private static $articleAuthors = [
        'Mike Ferengi',
        'Amy Oort',
    ];

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TagFixtures::class,
        ];
    }

    /**
     * @param ObjectManager $manager
     *
     * @return mixed|void
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Article::class,
            10,
            function () {
                $article = new Article();

                $article->setTitle($this->faker->randomElement(self::$articleTitles))
                    ->setAuthor($this->getRandomReference('main_users'))
                    ->setHeartCount($this->faker->numberBetween(5, 100))
                    ->setImageFilename($this->faker->randomElement(self::$articleImages))
                    ->setContent($this->faker->markdown());

                if ($this->faker->boolean(70)) {
                    $article->setPublishedAt(
                        $this->faker->dateTimeBetween('-100 days', '-1 days')
                    );
                }

                $tags = $this->getRandomReferences(Tag::class, $this->faker->numberBetween(0, 5));
                foreach ($tags as $tag) {
                    $article->addTag($tag);
                }

                return $article;
            }
        );

        $manager->flush();
    }
}
