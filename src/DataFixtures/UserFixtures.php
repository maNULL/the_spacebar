<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ApiToken;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Класс UserFixtures
 *
 * @package App\DataFixtures
 */
class UserFixtures extends BaseFixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * Конструктор класса UserFixtures
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return mixed|void
     */
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(
            'main_users',
            10,
            function (int $i) use ($manager) {
                $user = new User();

                $user->setEmail(sprintf('spacebar%d@example.com', $i))
                    ->setFirstName($this->faker->firstName)
                    ->setPassword($this->passwordEncoder->encodePassword($user, '123'))
                    ->agreeToTerms();

                if ($this->faker->boolean) {
                    $user->setTwitterUsername($this->faker->userName);
                }

                $apiToken1 = new ApiToken($user);
                $apiToken2 = new ApiToken($user);

                $manager->persist($apiToken1);
                $manager->persist($apiToken2);

                return $user;
            }
        );

        $this->createMany(
            'admin_users',
            3,
            function (int $i) use ($manager) {
                $user = new User();

                $user->setEmail(sprintf('admin%d@spacebar.com', $i))
                    ->setFirstName($this->faker->firstName)
                    ->setTwitterUsername($this->faker->userName)
                    ->setRoles(['ROLE_ADMIN'])
                    ->setPassword($this->passwordEncoder->encodePassword($user, '321'))
                    ->agreeToTerms();

                $apiToken1 = new ApiToken($user);
                $apiToken2 = new ApiToken($user);
                $apiToken3 = new ApiToken($user);

                $manager->persist($apiToken1);
                $manager->persist($apiToken2);
                $manager->persist($apiToken3);

                return $user;
            }
        );

        $manager->flush();
    }
}
