<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Класс TagFixtures
 *
 * @package App\DataFixtures
 */
class TagFixtures extends BaseFixture
{
    /**
     * @param ObjectManager $manager
     *
     * @return mixed|void
     */
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Tag::class,
            10,
            function () {
                $tag = new Tag();

                $tag->setName($this->faker->realText(20));

                return $tag;
            }
        );

        $manager->flush();
    }
}
