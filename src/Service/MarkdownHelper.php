<?php
declare(strict_types=1);

namespace App\Service;

use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Класс MarkdownHelper
 *
 * @package App\Service
 */
class MarkdownHelper
{
    /**
     * @var MarkdownInterface
     */
    private $markdown;
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var bool
     */
    private $isDebug;
    /**
     * @var Security
     */
    private $security;

    /**
     * Конструктор класса MarkdownHelper
     *
     * @param MarkdownInterface $markdown
     * @param AdapterInterface  $cache
     * @param LoggerInterface   $logger
     * @param Security          $security
     * @param bool              $isDebug
     */
    public function __construct(
        MarkdownInterface $markdown,
        AdapterInterface $cache,
        LoggerInterface $logger,
        Security $security,
        bool $isDebug
    )
    {
        $this->markdown = $markdown;
        $this->cache = $cache;
        $this->logger = $logger;
        $this->security = $security;
        $this->isDebug = $isDebug;
    }

    /**
     * @param string $source
     *
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function parse(string $source): string
    {
        if (false !== stripos($source, 'bacon')) {
            $this->logger->info(
                'They are talking about bacon again!',
                [
                    'user' => $this->security->getUser(),
                ]
            );
        }

        if ($this->isDebug) {
            return $this->markdown->transform($source);
        }

        $item = $this->cache->getItem('markdown_'.md5($source));
        if (!$item->isHit()) {
            $item->set($this->markdown->transform($source));
            $this->cache->save($item);
        }

        return $item->get();
    }
}