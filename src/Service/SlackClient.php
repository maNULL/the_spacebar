<?php
declare(strict_types=1);

namespace App\Service;

use App\Helper\LoggerTrait;
use Nexy\Slack\Client;

/**
 * Класс SlackClient
 *
 * @package App\Service
 */
class SlackClient
{
    use LoggerTrait;
    /**
     * @var Client
     */
    private $slack;

    /**
     * Конструктор класса SlackClient
     *
     * @param Client $slack
     */
    public function __construct(Client $slack)
    {
        $this->slack = $slack;
    }

    /**
     * @param string $from
     * @param string $message
     *
     * @throws \Http\Client\Exception
     */
    public function sendMessage(string $from, string $message)
    {
        $this->logInfo('Beaming a message to Slack!', ['message' => $message]);

        $message = $this->slack->createMessage()
            ->from($from)
            ->withIcon(':ghost:')
            ->setText($message);
        $this->slack->sendMessage($message);
    }
}