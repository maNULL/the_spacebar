<?php
declare(strict_types=1);

namespace App\Twig;

use App\Service\MarkdownHelper;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Класс AppExtension
 *
 * @package App\Twig
 */
class AppExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Конструктор класса AppExtension
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedServices(): array
    {
        return [
            MarkdownHelper::class,
        ];
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('markdown_cached', [$this, 'processMarkdown'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string $value
     *
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function processMarkdown(string $value): string
    {
        return $this->container->get(MarkdownHelper::class)->parse($value);
    }
}
