<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\CommentRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс CommentAdminController
 *
 * @IsGranted("ROLE_ADMIN_COMMENT")
 *
 * @package App\Controller
 */
class CommentAdminController extends AbstractController
{
    /**
     * @param Request            $request
     * @param CommentRepository  $commentRepository
     *
     * @param PaginatorInterface $paginator
     *
     * @return Response
     */
    public function index(
        Request $request,
        CommentRepository $commentRepository,
        PaginatorInterface $paginator
    ): Response {
        $queryBuilder = $commentRepository->getWithSearchQueryBuilder(
            $request->query->get('q')
        );

        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'comment_admin/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }
}
