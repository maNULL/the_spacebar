<?php
declare(strict_types=1);

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс AccountController
 *
 * @IsGranted("ROLE_USER")
 *
 * @package App\Controller
 */
class AccountController extends BaseController
{
    /**
     * @param LoggerInterface $logger
     *
     * @return Response
     */
    public function index(LoggerInterface $logger): Response
    {
        $logger->debug('Checking account page for '.$this->getUser()->getEmail());

        return $this->render(
            'account/index.html.twig',
            [
            ]
        );
    }

    /**
     * @return Response
     */
    public function accountApi(): Response
    {
        $user = $this->getUser();

        return $this->json($user, Response::HTTP_OK, [], ['groups' => ['main']]);
    }
}
