<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс AdminUtilityController
 *
 * @package App\Controller
 */
class AdminUtilityController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN_ARTICLE")
     *
     * @param Request        $request
     * @param UserRepository $userRepository
     *
     * @return Response
     */
    public function getUsersApi(Request $request, UserRepository $userRepository): Response
    {
        $users = $userRepository->findAllMatching($request->query->get('query'));

        return $this->json(
            [
                'users' => $users,
            ],
            200,
            [],
            ['groups' => ['main']]
        );
    }
}
