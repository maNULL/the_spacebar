<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс ArticleAdminController
 *
 * @package App\Controller
 */
class ArticleAdminController extends BaseController
{
    /**
     * @IsGranted("ROLE_ADMIN_ARTICLE")
     *
     * @param Request                $request
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ArticleFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('success', 'Article Created! Knowledge is power!');

            return $this->redirectToRoute('admin.article.list');
        }

        return $this->render(
            'article_admin/new.html.twig',
            [
                'articleForm' => $form->createView(),
            ]
        );
    }

    /**
     * @IsGranted("MANAGE", subject="article")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param Article                $article
     *
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $entityManager, Article $article): Response
    {
        $form = $this->createForm(
            ArticleFormType::class,
            $article,
            [
                'include_published_at' => true,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('success', 'Article updated! Inaccuracies squashed!');

            return $this->redirectToRoute('admin.article.edit', ['id' => $article->getId()]);
        }

        return $this->render(
            'article_admin/edit.html.twig',
            [
                'articleForm' => $form->createView(),
            ]
        );
    }

    /**
     * @param ArticleRepository $articleRepository
     *
     * @return Response
     */
    public function list(ArticleRepository $articleRepository): Response
    {
        return $this->render(
            'article_admin/list.html.twig',
            [
                'articles' => $articleRepository->findAll(),
            ]
        );
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getSpecificLocationSelect(Request $request): Response
    {
        if (!$this->isGranted('ROLE_ADMIN_ARTICLE') && $this->getUser()->getArticles()->isEmpty()) {
            $this->createAccessDeniedException();
        }

        $article = new Article();
        $article->setLocation($request->query->get('location'));

        $form = $this->createForm(ArticleFormType::class, $article);

        if (!$form->has('specificLocation')) {
            return new Response(null, Response::HTTP_NO_CONTENT);
        }

        return $this->render(
            'article_admin/_specific_location.html.twig',
            [
                'articleForm' => $form->createView(),
            ]
        );
    }
}
