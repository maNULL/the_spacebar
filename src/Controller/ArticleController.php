<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\SlackClient;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс ArticleController
 *
 * @package App\Controller
 */
class ArticleController extends AbstractController
{
    /**
     * @param ArticleRepository $articleRepository
     *
     * @return Response
     */
    public function homepage(ArticleRepository $articleRepository): Response
    {
        return $this->render(
            'article/homepage.html.twig',
            [
                'articles' => $articleRepository->findAllPublishedOrderedByNewest(),
            ]
        );
    }

    /**
     * @param Article     $article
     * @param SlackClient $slackClient
     *
     * @return Response
     * @throws \Http\Client\Exception
     */
    public function show(Article $article, SlackClient $slackClient): Response
    {
        if ($article->getSlug() === 'khaaaaaan') {
            $slackClient->sendMessage('Khan', 'Ah, Kirk, my old friend...');
        }

        return $this->render(
            'article/show.html.twig',
            [
                'article' => $article,
            ]
        );
    }

    /**
     * @param Article         $article
     * @param LoggerInterface $logger
     *
     * @return Response
     */
    public function toggleArticleHeart(Article $article, LoggerInterface $logger): Response
    {
        $article->incrementHeartCount();

        $this->getDoctrine()->getManager()->flush();

        $logger->info('Article is being hearted!');

        return $this->json(
            [
                'slug' => $article->getSlug(),
                'hearts' => $article->getHeartCount(),
            ]
        );
    }
}
