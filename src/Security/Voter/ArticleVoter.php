<?php
declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Article;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Класс ArticleVoter
 *
 * @package App\Security\Voter
 */
class ArticleVoter extends Voter
{
    /**
     * @var Security
     */
    private $security;

    /**
     * Конструктор класса ArticleVoter
     *
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, ['MANAGE'])
            && $subject instanceof Article;
    }

    /**
     * @param string         $attribute
     * @param Article        $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case 'MANAGE':
                if ($subject->getAuthor() === $user
                    || $this->security->isGranted('ROLE_ADMIN_ARTICLE', $user)
                ) {
                    return true;
                }

                break;
        }

        return false;
    }
}
