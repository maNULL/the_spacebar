<?php
declare(strict_types=1);

namespace App\Form\Model;

use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Класс UserRegistrationFormModel
 *
 * @package App\Form\Model
 */
class UserRegistrationFormModel
{
    /**
     * @Assert\NotBlank(message="Не должно быть пустым!!!")
     * @Assert\Email()
     * @UniqueUser()
     */
    public $email;
    /**
     * @Assert\NotBlank(message="Пароль надо указать обязательно!!!")
     * @Assert\Length(min=5, minMessage="Сложно придумать пароль длиннее 4 символов???")
     */
    public $plainPassword;
    /**
     * @Assert\IsTrue(message="Надо принять условия, если хотите зарегистрироваться!!!")
     */
    public $agreeTerms;
}