<?php
declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Класс UniqueUser
 *
 * @package App\Validator
 *
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 */
class UniqueUser extends Constraint
{
    public $message = 'Такой пользователь уже есть в БД!!!';
}
