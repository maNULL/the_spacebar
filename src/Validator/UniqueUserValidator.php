<?php
declare(strict_types=1);

namespace App\Validator;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Класс UniqueUserValidator
 *
 * @package App\Validator
 */
class UniqueUserValidator extends ConstraintValidator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Конструктор класса UniqueUserValidator
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        /* @var UniqueUser $constraint */

        $existingUser = $this->userRepository->findOneBy(['email' => $value]);

        if (!$existingUser) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->addViolation();
    }
}
